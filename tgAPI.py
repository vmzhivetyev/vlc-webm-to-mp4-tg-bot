import requests
import json
import time
from threading import Thread
from tools import log
import tools

class Message:
	def __init__(self, message):
		self.id = message['message_id']
		self.chatId = (message['chat']['id'])
		self.chatTitle = message['chat']['title'] if 'title' in message['chat'] else 'private chat'

		self.text 			= '' if message.get('text') is None else message.get('text')
		self.stickerId   	= message['sticker']['file_id']  	if 'sticker' 			in message else None
		self.stickerPack 	= message['sticker']['set_name']	if 'sticker' 			in message else None
		self.reply_to 	 	= message.get('reply_to_message')
		self.photoFileId	= message['photo'][-1]['file_id']	if 'photo'				in message else None
		self.videoFileId	= message['video']['file_id'] 		if 'video'				in message else None
		self.caption		= message.get('caption')
		self.document		= message.get('document')
		
		self.sender = User(message['from'])

class User:
	def __init__(self, user):
		self.id = (user['id'])
		self.first_name = user['first_name'] if 'first_name' in user else ''
		self.last_name = user['last_name'] if 'last_name' in user else ''
		self.username = user['username'] if 'username' in user else None
		self.hand = []
		self.pick = None
		self.select = None
		self.score = 1

	def call(self, name = None):
		if name is None:
			name = self.name()
		return '<a href="tg://user?id='+str(self.id)+'">'+name+'</a>'

	def name(self):
		return self.first_name + ' ' + self.last_name

class Telegram:

	offset = 0
	URL = 'https://api.telegram.org/bot'
	URLFILE = 'https://api.telegram.org/file/bot'
	TOKEN = tools.load_from_json('tg.auth', {'token':'none'})['token']
	listeners = []
	callback_listeners = []

	def __init__(self):
		self.chatMembers = tools.load_from_json('chatMembers.json', {})
		for strChatId in self.chatMembers.keys():
			for strUserId in self.chatMembers[strChatId].keys():
				self.chatMembers[strChatId][strUserId] = User(self.chatMembers[strChatId][strUserId])

	def inlineKeyboard(self, rowsOfButtons):
		keyboard = { 'reply_markup': json.dumps({ 'inline_keyboard': rowsOfButtons }, ensure_ascii=False)}

		return keyboard

	def getChatsList(self):
		return list(self.chatMembers.keys())

	def getChatMembers(self, chatId):
		if str(chatId) in self.chatMembers:
			return self.chatMembers[str(chatId)]
		return {}

	def addMessageListener(self, listener):
		self.listeners.append(listener)

	def addCallbackDataListener(self, listener):
		self.callback_listeners.append(listener)

	def removeListener(self, listener):
		if listener in self.listeners:
			self.listeners.remove(listener)
		if listener in self.callback_listeners:
			self.callback_listeners.remove(listener)

	def onUpdate(self, update):
		log("NEW UPDATE:\n", json.dumps(update, indent=4, sort_keys=True), "\n\n")

		if 'message' in update:
			chatId = str(update['message']['chat']['id'])
			members = []
			if 'new_chat_members' in update['message']:
				members = update['message']['new_chat_members']
			if 'from' in update['message'] and update['message']['from'] != '':
				members.append(update['message']['from'])
			for mem in members:
				if mem['is_bot']:
					continue

				mid = str(mem['id'])
				if not chatId in self.chatMembers:
					self.chatMembers[chatId] = {}
				if not mid in self.chatMembers[chatId]:
					log("append chat member",mem)
				self.chatMembers[chatId][mid] = User(mem)

			tools.log("Members: ",self.chatMembers,'\n')
			tools.save_to_json(self.chatMembers, 'chatMembers.json')

			for l in self.listeners:
				#try:
				msg = Message(update['message'])
				l(msg)
			#except Exception as ex:
			#	log("onUpdate can't call a listener, error:",ex)


		if 'callback_query' in update:
			for l in self.callback_listeners:
				l(update['callback_query']['message']['chat']['id'], update['callback_query']['data'], User(update['callback_query']['from']))

	def sendFile(self, chatId, filePath, caption=None,fileType='document',params=None):
		'''
		EXAMPLE:
			tg.sendFile('266627161', '15350489082120.webm', 'caption', 'video')

			It will take file at path 'data/15350489082120.webm'
		'''
		files = {
			fileType: open(filePath, 'rb'),
			'name' : filePath.split('/')[-1]
		}
		data = {
			'chat_id': chatId,
			'supports_streaming' : 'True',
		}
		if caption:
			data['caption'] = caption

		if params:
			data.update(params)

		methodUrl = self.URL+self.TOKEN+'/'+'send'+fileType.title()
		tools.log('======',methodUrl)
		request = requests.post(methodUrl, files=files, data=data)

		return request.json()
		

	def doMethod(self, methodName, data):
		try:
			methodUrl = self.URL+self.TOKEN+'/'+methodName
			#tools.log('DO METHOD URL:',methodUrl,'; data:',data)
			request = requests.post(methodUrl, data=data)
		except Exception as exception:
			tools.log('Send message error; data:',data,'\nException:',exception)
			return None

		if not request.status_code == 200:
			try:
				tools.log(methodName,' >> TG REQUEST ERROR: ',request.json()['description'], '\nRequest data: ', data)
			except:
				pass
			return None

		return request.json()

	def getFileInfo(self, fileId):
		fileInfo = self.getFile(fileId)
		fileInfo['url'] = self.URLFILE + self.TOKEN + '/' + fileInfo['file_path']
		tools.log('File info:',fileInfo)
		return fileInfo

	def getFile(self, fileId):
		data = {
			'file_id': fileId
		}

		return self.doMethod('getFile', data)['result']

	def getChat(self, chatId):
		data = {
			'chat_id': chatId
		}

		return self.doMethod('getChat', data)['result']

	def editMessageText(self, chatId, msgId, text):
		data = {
			'chat_id': chatId,
			'message_id' : msgId,
			'text' : text,
			'parse_mode' : 'HTML'
		}

		return self.doMethod('editMessageText', data)['result']


	def promoteChatMember(self, chatId, userId):
		data = {
			'chat_id': chatId,
			'user_id': userId,
			'can_change_info':True,
			'can_post_messages':True,
			'can_edit_messages':True,
			'can_delete_messages':False,
			'can_restrict_members':False
		}

		return self.doMethod('promoteChatMember', data)

	def exportChatInviteLink(self, chatId):
		return self.doMethod('exportChatInviteLink', {'chat_id':chatId})['result']

	def getMe(self):
		return User(self.doMethod('getMe', None)['result'])

	def kickChatMember(self, chatId, userId):
		if getMe.id == (userId):
			return

		data = {
			'chat_id': chatId,
			'user_id': userId,
			'until_date': 30
		}

		return self.doMethod('kickChatMember', data)

	def deleteMessage(self, chatId, msgId):
		data = {
			'chat_id': chatId,
			'message_id': msgId
		}

		return self.doMethod('deleteMessage', data)

	def sendSticker(self, chatId, sticker):
		if sticker is None:
			return False

		data = {
			'chat_id': chatId,
			'sticker': sticker
		}

		return self.doMethod('sendSticker', data)

	def sendPhoto(self, chatId, url, caption = None):
		data = {
			'chat_id': chatId,
			'photo': url,
			'parse_mode':'HTML'
		}

		if caption is not None:
			data['caption'] = caption

		return self.doMethod('sendPhoto', data)

	def sendVideo(self, chatId, url, caption = None):
		data = {
			'chat_id': chatId,
			'video': url,
			'parse_mode':'HTML'
		}

		if caption is not None:
			data['caption'] = caption

		return self.doMethod('sendVideo', data)

	def sendMessage(self, chatId, text, params = None):
		data = {
			'chat_id': chatId,
			'text':text,
			'parse_mode':'HTML'
			#'reply_to_message_id': update['message']['message_id'],
		}

		if params:
			data.update(params)

		return self.doMethod('sendMessage', data)

	def forwardMessage(self, chatId, msg):
		data = {
			'chat_id' : chatId,
			'from_chat_id' : msg.chatId,
			'message_id' : msg.id
		}

		return self.doMethod('forwardMessage', data)

	def check_updates(self):
		data = {'offset': self.offset+1, 'limit': 0, 'timeout': 1}
		response_obj = self.doMethod('getUpdates', data)

		if response_obj and 'result' in response_obj:
			for update in response_obj['result']:
				self.offset = update['update_id']
				self.onUpdate(update)

	def work(self):
		while True:
			try:
				self.check_updates()
			except KeyboardInterrupt:
				log('Interrupted by the user')
				break