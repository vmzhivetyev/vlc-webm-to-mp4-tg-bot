from datetime import datetime
from urllib.request import urlopen
import codecs
import json
import requests
import random
import time
import os
from json import JSONEncoder
from bs4 import BeautifulSoup

from multiprocessing import Queue
from threading import Thread

class MyEncoder(JSONEncoder):
	def default(self, o):
		return o.__dict__

class ThreadSafeLogger:

	def __init__(self, file_name, encoding = 'utf-8', mode = 'a'):
		self.file_name = file_name
		self.write_queue = Queue()
		self.encoding = encoding
		self.mode = mode

		self.thread = Thread(target = self.thread_func)
		self.thread.start()

	def thread_func(self):
		while True:
			while not self.write_queue.qsize():
				time.sleep(0.1)

			with codecs.open(self.file_name, encoding=self.encoding, mode=self.mode) as f:
				while self.write_queue.qsize():
					f.write(self.write_queue.get() + '\n\n')

	def log(self, *tup):
		s = ''
		for i in tup:
			s += str(i) + ' '

		if s is not '':
			self.write_queue.put(s)

file_logger = ThreadSafeLogger('data/log.txt')
def log(*tup): 
	pref = str(datetime.now()) + ': '
	try:
		print(pref, *tup, "\n")
	except Exception:
		print(pref, "a very strange log")

	try:
		file_logger.log(pref, *tup)
	except Exception as ex:
		print('Exception occured with log-file: ', ex)

def try_parse_int(s, base=10, val=None):
	try:
		return int(s, base)
	except ValueError:
		return val

def random_quote():
	json = requests.post('https://citaty.info/ajax/random_quote/0/po/0/0/json').json()
	return json.split('<p>')[1].split('</p>')[0].replace('&nbsp;',' ')

def save_to_json(obj, file):
	file = 'data/'+file
	#try:
	data = json.dumps(obj, cls=MyEncoder, indent=4, sort_keys=True, ensure_ascii=False)
	f = codecs.open(file, encoding='utf-8', mode='w')
	f.write(data)
	f.close()
	log('saved json file',file)
	#except Exception as ex:
	#	log("Can't save json file",file,"\nException: ",ex)

def load_from_json(file, val = None):
	file = 'data/'+file
	try:
		f = codecs.open(file, encoding='utf-8', mode='r')
		obj = json.loads(f.read())
		f.close()

		if isinstance(val, dict):
			for k in val.keys():
				if not k in obj:
					obj[k] = val[k]

		return obj
	except Exception as ex:
		log("Can't load obj from file",file,"\nException: ",ex)
		return val

def rand(lst):
	if len(lst) == 0:
		return None
	return lst[random.randint(0,len(lst)-1)]

def getSoup(url):
	html_doc = requests.get(url).text
	soup = BeautifulSoup(html_doc, 'html.parser')
	return soup

def downloadFile(url, _file=None):
	if _file is None:
		_file = url.split('/')[-1]
	file = 'video/'+_file

	exception = None
	try:
		request = urlopen(url)
		data = request.read()

		with open(file, 'wb') as f:  
			f.write(data)
	except Exception as ex:
		exception = ex

	savedFilePath = _file if exception is None else None
	# either exception=None or fullPath=None
	return exception, savedFilePath

def fileData(path):
	path = 'data/'+path
	data = None
	with open(path, 'rb') as f:  
		data = f.read()
	return data