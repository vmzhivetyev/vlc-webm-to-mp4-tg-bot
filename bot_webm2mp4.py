from tgAPI import Telegram
from subprocess import Popen
from threading import Thread
import tools
import re
import subprocess
import os
import time

tg = Telegram()
games = {}


class ThreadMessageHandler:

	def __init__(self, msg):
		self.msg = msg
		self.thread = Thread(target = self.thread_func)
		self.thread.start()

	def thread_func(self):
		global tg

		msg = self.msg

		if msg.document is None:
			return

		fileId = msg.document["file_id"]
		fileName = msg.document["file_name"]

		if fileName.split('.')[-1] != 'webm':
			return

		params = {'reply_to_message_id':msg.id}

		logId = tg.sendMessage(msg.chatId, '⏳ Downloading...', params)['result']['message_id']

		# download
		info = tg.getFileInfo(fileId)
		error,path = tools.downloadFile(info['url'], fileName)
		tools.log(error,path)

		# naming
		mp4File = re.sub(r'\.webm$','.mp4',path)
		sourceFile = 'video/'+path
		targetFile = 'video/'+mp4File

		# convert
		tg.editMessageText(msg.chatId, logId, '⌛ Converting...')

		subprocess.Popen(['webm_to_mp4.bat','video/'+path], shell=True).wait()
		os.remove(sourceFile)

		if os.path.exists(sourceFile+'.mp4'):
			if os.path.exists(targetFile):
				os.remove(targetFile)

			os.rename(sourceFile+'.mp4', targetFile)

			#send
			tg.editMessageText(msg.chatId, logId, '🏁 Sending...')

			try:
				result = tg.sendFile(msg.chatId, targetFile, fileType='video', params=params)
				tools.log('Converted and sended:',result)
			except:
				tg.editMessageText(msg.chatId, logId, '😰 Ooops, something went wrong')

		tg.deleteMessage(msg.chatId, logId)


def onMessage(msg):
	ThreadMessageHandler(msg)

if __name__ == "__main__":
	print('Starting...')

	tg.addMessageListener(onMessage)
	tg.work()